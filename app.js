const express = require('express');
const storage = require('node-persist');
const app = express();
require('dotenv').config()

app.get('/guess', async (req, res) => {
	res.setHeader('Access-Control-Allow-Origin', process.env.HOST);
	
	//DECLARE VARIABLES
	let player = parseInt(req.query.player);
	let number = parseInt(req.query.number);
	let message = ""

	//VALIDATE
	if( isNaN(player) || isNaN(number) ){
		res.status(400).send()
	}
	if( number < 0 || number > 100 ){
		res.status(400).send({ "message" : "not a valid number" })
	}
	if(	player < 0 ){
		res.status(400).send({ "message" : "not a valid player" })
	}

	//INIT STORAGE
	await storage.init();
	
	//CHECK NUMBER IN STORAGE
	let snapNumber = await storage.getItem(`player${player}`)
	if(!snapNumber){
		let random = Math.floor( Math.random() * 101 ) // Random number 0 <= n < 101 
		await storage.setItem(`player${player}`, random)
		snapNumber = random
	}

	//PERFORM -> COMPARISON
	if(number === snapNumber){ 		
		message = "Bingo!!";
	}else if(number < snapNumber){ 		
		message = "higher";
	}else if(number > snapNumber){ 		
		message = "lower"
	}

	//RESPONSE
  	res.status(200).send({
    	guess: message,
    	player
  	})
});


app.post('/resetGame',  async (req, res) => {
	//INIT STORAGE
	await storage.init()

	//PERFORM -> COMPARISON
	await storage.clear();

	//RESPONSE
  	res.setHeader('Access-Control-Allow-Origin', process.env.HOST);
  	res.sendStatus(204)
})

app.use(function (req, res, next) {
    res.status(404).send()
})


const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
});

