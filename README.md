# HelloPrint Code Challenge - Multiplayer Game API
## Backend

### Requirements
1) Node Version 8.9.4


### Instructions
The project needs NodeJs

1) Open a new **terminal** in Mac
2) Go to the directory you wish to download the project
3) Clone the repository by copy and pasting in the terminal the following command <br/>
    **git clone https://gitlab.com/efsanchezus/helloprint_codechallenge_back.git**
4) Go to the project by typing **cd helloprint_codechallenge_back**
5) Type **npm install**
6) Run **node app.js**	
7) It must print server running on port 5000
8) Now it's **time to play**!

### Structure
2 endpoints 

**GET** : /guess accepts two parameters in the URL "number" and "player" -> returns { guess, message } <br/>
**POST** : /resetGame to clear storage for a new game -> returns 204 

### Dependencies

1) babel-cli
2) babel-preset-es2015 
3) dotenv
4) express 
5) node-persist




